package basics;

import java.util.Scanner;

public class FloydTriangle {

	public static void main(String[] args) {
		System.out.println("Enter the number of rows: ");
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		int rows = sc.nextInt();
		
		int x=1;
	    
		for(int i=1;i<=rows;i++) {
			for(int j=1;j<=i;j++) {
				System.out.print(x+" ");
				x++;
			}
			System.out.println();
		}

	}

}
