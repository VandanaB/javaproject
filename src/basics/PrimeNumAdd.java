package basics;

public class PrimeNumAdd {

	public static void main(String[] args) {
		String a ="13567481";
		int sum=0;
		char[] ch = a.toCharArray();
		for (char c : ch) {
			int num = Character.getNumericValue(c);
			//System.out.print(c);
			int count =0;
			for(int i=1; i<=num; i++) {
				if(num%i==0) {
					count++;
				}		
			}
			if (count==2) {
				System.out.println("Prime Number: "+num);
				sum=sum+num;		
			}
		}System.out.println("Adding of prime numbers is "+sum);
	}
}
