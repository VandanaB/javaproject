package basics;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class ReverseNum {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the value: ");
		Integer nextInt = sc.nextInt();
		
		String strNum = nextInt.toString();
		String[] numArry = strNum.split("");
		
		List<String> lst =  Arrays.asList(numArry);
		Collections.reverse(lst);
		Set<String>  lst1 = new LinkedHashSet<>();
		lst1.addAll(lst);
		System.out.println(lst1);
		String str = lst1.toString();
		String str1 = str.replaceAll("[^0-9]", "");
		int num = Integer.parseInt(str1);
		System.out.println(num);
        
	}

}
