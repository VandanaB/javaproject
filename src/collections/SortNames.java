package collections;


import java.util.Set;
import java.util.TreeSet;

public class SortNames {

	public static void main(String[] args) {
		Set<String> names = new TreeSet<String>();
		names.add("John");
		names.add("Mary");
		names.add("Neela");
		names.add("Peppa");
		names.add("Anna");
		
		for (String name : names) {
			System.out.println(name);
			
		}
		

	}

}
